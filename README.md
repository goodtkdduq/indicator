# Indicator
### 해당 코드는 upbit의 데이터 가져와만든 Test 코드입니다.
## 기본 데이터 ( 5,10,20 봉 평균 이동선 )

**info = Indicator(data=df)**

``` python
from indicator import  Indicator
import pyupbit
# 기본 데이터 가져오기 (시가 고가 저가 종가)
df = pyupbit.get_ohlcv('KRW-BTC', interval="day", count=200)
# 각 컬럼의 명칭 변경
# open -> 시가
# high -> 고가
# low -> 저가
# close -> 종가
# volume -> 거래량
df.rename(columns={'open': '시가'}, inplace=True)
df.rename(columns={'high': '고가'}, inplace=True)
df.rename(columns={'low': '저가'}, inplace=True)
df.rename(columns={'close': '종가'}, inplace=True)
df.rename(columns={'volume': '거래량'}, inplace=True)

info = Indicator(data=df)
print(info.data['5'])
print(info.data['10'])
print(info.data['20'])
print(info.data['60'])
```
## 기본 데이터 결과
![결과](/IMG/df_basic.JPG)


## RSI 기본 코드
Indicator 호출 시 RSI 파라미터를 True로 전달  
**info = Indicator(data=df,RSI=True)**
``` python
from indicator import  Indicator
import pyupbit
df = pyupbit.get_ohlcv('KRW-BTC', interval="day", count=200)	# 기본 데이터 가져오기 (시가 고가 저가 종가)
df.rename(columns={'open': '시가'}, inplace=True)	# open -> 시가
df.rename(columns={'high': '고가'}, inplace=True)	# high -> 고가
df.rename(columns={'low': '저가'}, inplace=True)	# low -> 저가
df.rename(columns={'close': '종가'}, inplace=True)	# close -> 종가
df.rename(columns={'volume': '거래량'}, inplace=True)	# volume -> 거래량
info = Indicator(data=df,RSI=True)
print(info.data['RSI'])
```
## RSI 결과
기본 데이터 + RSI  
![결과](/IMG/df_RSI.JPG)

## Williams %R 기본 코드
Indicator 호출 시 WR 파라미터를 True로 전달  
**info = Indicator(data=df,WR=True)**
``` python
from indicator import  Indicator
import pyupbit

# 기본 데이터 가져오기 (시가 고가 저가 종가)
df = pyupbit.get_ohlcv('KRW-BTC', interval="day", count=200)

# 각 컬럼의 명칭 변경
# open -> 시가
# high -> 고가
# low -> 저가
# close -> 종가
# volume -> 거래량
df.rename(columns={'open': '시가'}, inplace=True)
df.rename(columns={'high': '고가'}, inplace=True)
df.rename(columns={'low': '저가'}, inplace=True)
df.rename(columns={'close': '종가'}, inplace=True)
df.rename(columns={'volume': '거래량'}, inplace=True)

info = Indicator(data=df,WR=True)
print(info.data['WR'])
```
## Williams %R 결과
기본 데이터 + Williams %R  
![결과](/IMG/df_WR.JPG)


## SuperTrend 기본 코드
Indicator 호출 시 SuperTrend 파라미터를 True로 전달  
**info = Indicator(data=df,SuperTrend=True)**
``` python
from indicator import  Indicator
import pyupbit

# 기본 데이터 가져오기 (시가 고가 저가 종가)
df = pyupbit.get_ohlcv('KRW-BTC', interval="day", count=200)

# 각 컬럼의 명칭 변경
# open -> 시가
# high -> 고가
# low -> 저가
# close -> 종가
# volume -> 거래량
df.rename(columns={'open': '시가'}, inplace=True)
df.rename(columns={'high': '고가'}, inplace=True)
df.rename(columns={'low': '저가'}, inplace=True)
df.rename(columns={'close': '종가'}, inplace=True)
df.rename(columns={'volume': '거래량'}, inplace=True)

info = Indicator(data=df,SuperTrend=True)
print(info.data['ST'])
```
## SuperTrend 결과
기본 데이터 + SuperTrend  
![결과](/IMG/df_ST.JPG)
![결과2](/IMG/trend_ST.JPG)
