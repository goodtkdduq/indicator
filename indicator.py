import traceback
from pandas import Timedelta
import math


class Indicator:
    moveLine = {}
    moveLineForVolume = {}

    def __init__(self, data=None ,reverse=True, useVolume=False,RSI=False,STDEV=False,ATR=False,SuperTrend = False,WR=False):
        self.moveLine.clear()
        self.moveLineForVolume.clear()
        self.data = data
        self.data['5'] = None
        self.data['10'] = None
        self.data['20'] = None
        self.data['60'] = None
        self.data['120'] = None
        self.data['diff']=None
        self.data['5to10']=None
        self.data['10to20'] = None

        if reverse == True:
            self.data.sort_index(ascending=True, inplace=True)
        # 종가 데이터
        self.list = []
        real = self.getRealData()
        self.list.append([real, '^r--'])

        # 500초 평균선
        first = self.makeMeanData(5)
        self.list.append([first, 'g'])

        # 5000초 평균선
        sec = self.makeMeanData(10)
        self.list.append([sec, 'b'])

        # 20일 평균선

        th = self.makeMeanData(20)
        self.list.append([th, 'g'])

        th = self.makeMeanData(60)
        self.list.append([th, 'g'])

        th = self.makeMeanData(120)
        self.list.append([th, 'g'])

        if useVolume == True:
            self.volume = []
            self.data['5 거래량'] = None
            self.data['10 거래량'] = None
            self.data['20 거래량'] = None

            th = self.makeMeanVolume(5)
            self.volume.append([th, 'g'])
            th = self.makeMeanVolume(10)
            self.volume.append([th, 'b'])
            th = self.makeMeanVolume(20)
            self.volume.append([th, 'g'])
            self.makeMoveLineForVolume(self.volume)

        if RSI == True:
            self.makeRsi()
        if STDEV==True:
            self.makeSTDEV()
        if ATR==True:
            self.makeTR()
        if SuperTrend ==True:
            self.makeTR()
            self.makeSuperTrend()

        if WR==True:
            self.makeWR()
        self.makeMoveLine(self.list)



    def makeRsi(self,N=14):
        self.data['RSI'] = None
        preUp =0
        preDown =0
        #print(self.data)
        #print(len(self.data))
        for i in range(N,len(self.data)):
            up = 0
            down = 0
            upCnt =0
            downCnt=0
            for j in range(i-N+1,i+1):
                stand = self.data['종가'][j - 1]
                now =  self.data['종가'][j] - stand
                if now > 0:
                    up+=now
                    upCnt+=1
                elif now < 0:
                    down+=abs(now)
                    downCnt+=1



            if preUp !=0 and preDown !=0:

                ## 추가 로직
                pre = self.data['종가'][i - 1]
                now = self.data['종가'][i]
                gain = now - pre
                if gain > 0:
                    up = gain
                    down = 0
                else:
                    up = 0
                    down=abs(gain)
                upA = (preUp*(N-1)+up)/N
                downA = max((preDown*(N-1)+down)/N,0.000001)
            else:
                upA = up/N
                downA = max(down/N,0.000001)

            rs = upA/downA
            rsi = 100-100/(1+rs)
            self.data.loc[self.data.index[i],'RSI'] =rsi
            preUp = upA
            preDown = downA

    def makeSTDEV(self,N=14):
        self.data['STDEV'] = None
        for i in range(N, len(self.data)):
            sum =0
            for j in range(i - N, i):
                sum+=(self.data['종가'][j]**2)
            var = sum/N
            stdev = math.sqrt(var)
            self.data.loc[self.data.index[i],'STDEV']= stdev

    def makeTR(self,p = 7):
        self.data['TR'] = None
        for i in range(2, len(self.data)):
            TR = max(abs(self.data['고가'][i]-self.data['종가'][i-1]),abs(self.data['저가'][i]-self.data['종가'][i-1]))
            TR = max(TR,self.data['고가'][i] - self.data['저가'][i])
            self.data.loc[self.data.index[i], 'TR'] = TR

        self.makeATR(p)

    def makeATR(self, p=7):
        self.data['ATR'] = None
        ATR = self.data['TR'].rolling(window=p).mean()
        for i in ATR.keys():
            self.data.loc[i, 'ATR'] = ATR[i]

    def makeSuperTrend(self,multiple=3):
        self.data['ST'] = None
        upper ={}
        lower={}
        for i in range(len(self.data)):
            up = ((self.data['고가'][i] + self.data['저가'][i]) / 2) + multiple * self.data['ATR'][i]
            low = ((self.data['고가'][i] + self.data['저가'][i]) / 2) - multiple * self.data['ATR'][i]
            if math.isnan(up):
                upper[i]=0
            else:
                if up < upper[i-1] or self.data['종가'][i-1] > upper[i-1]:
                    upper[i] = up
                else:
                    upper[i] = upper[i-1]

            if math.isnan(up):
                lower[i] =0
            else:
                if low > lower[i-1] or self.data['종가'][i-1] < lower[i-1]:
                    lower[i] = low
                else:
                    lower[i] = lower[i-1]


        for i in range(len(self.data)):
            if self.data.loc[self.data.index[i], 'TR'] != None:

                if self.data['ST'][i-1] == upper[i-1] and self.data['종가'][i] <= upper[i]:
                    st = upper[i]
                elif self.data['ST'][i-1] == upper[i-1] and self.data['종가'][i] > upper[i]:
                    st = lower[i]
                elif self.data['ST'][i-1] == lower[i-1] and self.data['종가'][i] >= lower[i]:
                    st = lower[i]
                elif self.data['ST'][i-1] == lower[i-1] and self.data['종가'][i] < lower[i]:
                    st = upper[i]
                self.data.loc[self.data.index[i], 'ST'] = st
            else:
                self.data.loc[self.data.index[i], 'ST'] = 0

    def makeWR(self,p=14):
        self.data['WR'] = None
        for i in range(p, len(self.data)):
            max=0
            min=9999999999

            for j in  range(i-p+1,i+1):
                print(i,j)
                if self.data['고가'][j] > max:
                    max = self.data['고가'][j]
                if self.data['저가'][j] < min:
                    min =self.data['저가'][j]
            WR = ((max-self.data['종가'][i]) / (max-min)) * (-100)
            self.data.loc[self.data.index[i], 'WR'] = WR


    def makeMoveLine(self, list):

        for i in list:
            for j in i[0].keys():
                if self.moveLine.__contains__(j):
                    self.moveLine[j].append(i[0][j])
                else:
                    self.moveLine[j] = [i[0][j], ]

        for i in self.moveLine:
            self.data.loc[i, '5'] = self.moveLine[i][1]
            self.data.loc[i, '10'] = self.moveLine[i][2]
            self.data.loc[i, '20'] = self.moveLine[i][3]
            self.data.loc[i, '60'] = self.moveLine[i][4]
            self.data.loc[i, '120'] = self.moveLine[i][5]
            self.data.loc[i, '5to10'] = float(self.moveLine[i][1])-float(self.moveLine[i][2])
            self.data.loc[i, '10to20'] = float(self.moveLine[i][2]) - float(self.moveLine[i][3])
            self.data.loc[i,'diff'] = abs(self.data.loc[i,'종가']-self.data.loc[i,'시가'])

    def makeMoveLineForVolume(self, list):

        for i in list:
            for j in i[0].keys():
                if self.moveLineForVolume.__contains__(j):
                    self.moveLineForVolume[j].append(i[0][j])
                else:
                    self.moveLineForVolume[j] = [i[0][j], ]

        for i in self.moveLineForVolume:
            self.data.loc[i, '5 거래량'] = self.moveLineForVolume[i][1]
            self.data.loc[i, '10 거래량'] = self.moveLineForVolume[i][2]
            # self.data.loc[i, '20 거래량'] = self.moveLine[i][3]

    def returnMoveLine(self):
        return self.moveLine

    def currentBuyAndSell(self, pre, cur):

        # 파는 조건(매도)
        if self.isRealRightUp(pre, cur) and self.is5DayMeanRightUp(pre, cur) and self.isUP5to10(cur):
            return 10
        # 사는 조건 (매수)
        elif (self.isRightDown(pre, cur) and self.is5DayMeanRightDown(pre, cur) and self.isDown5to10(cur)) or (
        self.isDecrease(pre, cur)):
            return -10
        else:
            return 0


    def superTrendBackTest(self):
        try:
            listDict = {}
            df1 = self.data
            price = df1['종가'][0]

            sellList = []
            sellList.append(None)
            sellTime=[]
            sellTime.append(None)

            buyList = []
            buyList.append(None)
            buyTime=[]
            buyTime.append(None)
            # 현재 샀으면 True 팔았으면 False
            st = True
            buyPrice = 0
            buyTm = df1.index[-1]

            moveLine = '60'
            print(len(df1))
            for i in range(50,len(df1)):
                # 사는 조건
                df = self.data[i:i+1]
                # 매수
                if df['ST'][-1] < df['종가'][-1] and \
                    st == True:
                    t = Timedelta(hours=9)
                    sellList.append(df['종가'][-1])
                    sellTime.append(df.index[-1])
                    buyTime.append(None)
                    buyList.append(None)
                    st = False
                    buyPrice = int(df['종가'][-1])
                    buyTm = df.index[-1]

                # 익절
                elif df['ST'][-1] > df['종가'][-1] and st == False:
                    buyList.append(df['종가'][-1])
                    sellList.append(None)
                    sellTime.append(None)
                    buyTime.append(df.index[-1])
                    st = True


                else:
                    sellList.append(None)
                    buyList.append(None)
                    sellTime.append(None)
                    buyTime.append(None)



            ## 매수 매도 확인
            isSell = False
            tot = 0
            cell = 0
            totCnt = 0

            for i in range(len(sellList)):
                if isSell == False and sellList[i] == None:
                    continue
               # elif isSell == False and sellList[i] != None:
                elif sellList[i] != None:
                    print()
                    print("------- 매 수 -------")
                    t = Timedelta(hours=9)
                    print("매수일 : ", sellTime[i]+t)
                    print("매수금 : ", sellList[i])
                    cell = int(sellList[i])
                    isSell = True
                elif isSell == True and buyList[i] != None:
                    print("------- 매 도 -------")
                    t = Timedelta(hours=9)
                    print("매도일 : ", buyTime[i] + t)
                    print("매도금 : ", buyList[i])
                    #print("투자일 : ", int(keyList[i]) - int(cellDay))
                    susu = int(buyList[i]) * 0.0005

                    cell = int(buyList[i]) - cell - susu
                    print("금번 손익 : ", cell)
                    tot = tot + cell
                    totCnt = totCnt + 1
                    isSell = False
                else:
                    continue
            print("TOT : ", tot)
            print("거래 횟수 :", totCnt)
            print("수익률 :",round(tot/(price/100),2))
            #print("CAGR :",(price+tot/price)-1)
            return tot,totCnt,round(tot/(price/100),2)
        except:
            print(traceback.format_exc())

    def getRealData(self):
        # 종가 데이터
        ret = self.data['종가']
        ret.__setattr__("name", "real")
        return ret

    def makeMeanData(self, standard):
        # standard (5일,10일,20일 etc...)에 맞게 rolling 후 평균 산출
        mean = self.data['종가'].rolling(window=standard).mean()
        mean.__setattr__("name", str(standard))
        return mean

    def makeMeanVolume(self, standard):
        # standard (5일,10일,20일 etc...)에 맞게 rolling 후 평균 산출
        mean = self.data['거래량'].rolling(window=standard).mean()
        mean.__setattr__("name", str(standard))
        return mean